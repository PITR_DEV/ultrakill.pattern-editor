import styled, { css } from "styled-components";
import { Link } from "react-router-dom";
import { QuickMargins, Dockable } from "./Layout";
import { BlockColor, BlockTextColor } from "../helpers/ColorHelper";

export const WithActiveState = css`
  ${(props) =>
    props.active &&
    `
  background-color: white;
  border-color: white !important;
  color: black;
  font-weight: 600;
  &:focus { background-color: white; }
`}
`;

export const WithFadedState = css`
  ${(props) =>
    props.faded &&
    `opacity: 0.5;`}
`;

export const PrimaryButton = styled.button`
  font-family: "VCR OSD";
  font-size: 18px;
  border: 2px solid white;
  border-radius: 6px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 20px;
  padding-right: 20px;
  background-color: unset;
  color: white;
  outline: none;
  cursor: pointer;
  ${(props) => !props.small && `min-width: 100px;`}
  ${(props) => props.wide && "min-width: 200px;"}
  ${Dockable}
  ${QuickMargins}

  transition: border 150ms ease-in-out, background-color 100ms ease-in-out, color 100ms ease-in-out;

  &:focus,
  &:hover {
    border: 2px solid red;
  }

  ${WithActiveState}
  ${WithFadedState}

  &:active {
    background-color: red;
    border: 2px solid red;
    color: white;
  }
`;

export const PrefabButton = styled.button`
  font-family: "VCR OSD";
  font-size: 18px;
  border: 2px solid white;
  border-radius: 6px;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-left: 10px;
  padding-right: 10px;
  margin-bottom: 10px;
  background-color: unset;
  color: white;
  outline: none;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  align-items: center;
  place-content: space-between;
  ${(props) => !props.small && `min-width: 100px;`}
  ${(props) => props.wide && "min-width: 200px;"}
  ${QuickMargins}

  transition: border 150ms ease-in-out, background-color 100ms ease-in-out, color 100ms ease-in-out;

  &:focus,
  &:hover {
    border: 2px solid red;
  }

  ${WithActiveState}
  ${WithFadedState}

  &:active {
    background-color: red;
    border: 2px solid red;
    color: white;
  }

  & img {
    max-height: 32px;
  }
`;

// uugh, mostly duplicated code
export const PrimaryButtonLink = styled(Link)`
  font-family: "VCR OSD";
  font-size: 18px;
  text-decoration: none;
  text-align: center;
  border: 2px solid white;
  border-radius: 6px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 20px;
  padding-right: 20px;
  background-color: unset;
  color: white;
  outline: none;
  cursor: pointer;
  ${(props) => !props.small && `min-width: 100px;`}
  ${(props) => props.wide && "min-width: 200px;"}
  ${Dockable}
  ${QuickMargins}
  box-sizing: border-box;

  transition: border 150ms ease-in-out, background-color 100ms ease-in-out;

  &:focus,
  &:hover {
    border: 2px solid red;
  }

  ${WithActiveState}

  &:active {
    background-color: red;
    border: 2px solid red;
    color: white;
  }
`;

export const Tab = styled.a`
  margin: 0px 5px 15px 5px;
  font-size: 24px;
  text-decoration: none;
  text-align: center;
  border: 2px solid white;
  border-radius: 5px;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 15px;
  padding-right: 15px;
  background-color: unset;
  color: white;
  outline: none;
  cursor: pointer;
  min-width: 200px;
  box-sizing: border-box;
  ${Dockable}

  ${(props) => props.wide && "min-width: 200px;"}

  ${(props) => props.docked === "top" && "margin-top: 0px;"}

  transition: border 150ms ease-in-out, background-color 100ms ease-in-out;

  &:focus,
  &:hover {
    border: 2px solid red;
  }

  ${WithActiveState}

  &:active {
    background-color: red;
    border: 2px solid red;
    color: white;
  }
`;

export const GridButton = styled.button.attrs((props) => ({
  style: {
    backgroundColor: BlockColor(props.height),
    color: BlockTextColor(props.height, props.prefab === "0"),
  },
}))`
  width: 100%;
  position: relative;
  border: 0.5px solid rgba(255, 255, 255, 0.3);
  outline: none;
  overflow: hidden;
  //font-size: 1.5vw;
  font-size: 13px;

  &:hover {
    background-color: rgb(230, 230, 230);
  }

  &:hover {
    border-color: rgba(255, 255, 255, 1);
  }

  &:active {
    border-color: red;
  }

  ${(props) =>
    props.selected &&
    `
    background-color: red !important;
    &:focus { background-color: red; }

    color: black !important;
    font-weight: 600;
  `}
`;
