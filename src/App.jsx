import React from "react";

import { BrowserRouter as Router, Route } from "react-router-dom";
import { AnimatedSwitch } from "react-router-transition";
import DragArea from "./components/DragArea";
import routes from "./routes";
import { MobileBlocker, OnlyDesktop } from "./style/Layout";

class App extends React.Component {
  state = {
    dragCounter: 0,
  };

  componentDidMount() {
    document.addEventListener("dragenter", (event) => {
      this.setState({ dragCounter: this.state.dragCounter + 1 });
      console.log("Enter");
    });
    document.addEventListener("dragleave", (event) => {
      this.setState({ dragCounter: this.state.dragCounter - 1 });
      console.log("Leave");
    });
    document.addEventListener("drop", (event) => {
      this.setState({ dragCounter: 0 });
      console.log("Drop");
    });
    global.Happiness = () => {
      console.log(`
      ---____,,,::!===~~~;><<<^^<;~========~=~~~;~;;~;<;;<<^^^****r)rr
      ---_______,:!!===~~~;>;<^^^<~=!!!!!====~~~~=~=~~~~~~~><>^^*****r
      --_________,:!===~~~~~;<<<<<~=!!!!!!!===!======>)]v~~~>><<^^^***
      -.----_=r*!_":!!====~~;;;><<~=!!!!!!!!!!!!!!==rTyjkr==~~;<^^^^^*
      ------:TzjVx~::!!=====~~~><<~=!:::::::::::::>xcVVVVx===~=~~>>>^^
      ------!}VywwwL~:!======~~;<>~=!:"::,":::::!)cVVcuccv!!!!!=~=;;>^
      -...--,?i}VwkzzL^==~=====~~;~=!:",,,,":":*TkkyVuuTl^::!!:!===~~;
      .....--<}YcykjIK3Vr~~=~~~=~<<=!::":=!!=*TUmIzyVcV}x!,":"::!!!!~=
      ......-,LYTcwjs3HMqolLTjVii}uVwwkVsMPsseK33eIIwyy}(_____",::::=!
      .-..-.-.*TVyyjmPGGPKmU3GmPIoddRZPKd3qGKUUXIUememXj~-----_,,":::!
      ........_uUK3m33mUjosHMIV3OMO88RMd6KIqbPyyjzjXUesy=.'.-.-__,,,::
      ......''.rjmmmIXIXkz3OWzkqbZR00ObZOd3KbOPPMMW3mmKe)''''...-__,,:
      ......'''rXeowwIHZdZdOW3ZOMMdOOddd9O5KM$B###BQ$6O5},'''''...-__,
      ....'''''^zUs36QB#@@#B6K3MMHWZO66OROmGB#@@@@#BQ966dk!'''''...-_,
      ...'''''_}WMPK$8B#@@@#BOykKmPHb6OOOOqE##@@@##B9MZZWPV-  '''.'.-_
      ..'''''')KeUUXPg##@@#BQ8ZWPHqd900$$$$QBQQBBQgER0EbWmX_   ''''.._
      .'''''''vkeWdOOO60$RZPdD$R666ED0$$8BQQBQ00g8g06dZqmUw_    '''..-
      ..''''''~yI33PMdO6RROdODD00D6R0$$g8QQQQQg$00D9dM5PUoc_     '''..
      ''''''''~TyjeKGZbZqqMZdO60DOR$g88g8QQQ8g$0RbqWG3soIo}'     ''''.
      ''''    =uIKKPH3XkIP5MddMZO$8Q$0$8#BgEO6EOdGUwVVwzwy^     ''''.-
      ''''  ''=LVXIUmoVlVwXKGZMmzzPDg88Q0M33qdOZMPskykIIIo}_     '''..
      '''''''':xcywzokT}TVykoIzjkXmM6g$OMGPPGWMWW3ezyjemUm3w~' ''''..-
      '''      "iwzooyuTVyyoIeI3KWZdOD$6dZMGqGP3KsXXeKIkj333Uv-''...-_
      ''       '=TVykwyVccVze3WZdOD$8Q88g$DR6bGmIXseKKm3MddZWKT~,_____
      '''      .rcyyywkyVVVVXPd6OOOddZZZMqG333mIeKsssmWb6RDDD6ZmL!",":
      ''''   '_xyjsszVcVVVVywwzXIeeem3HKUIzkzjIseeemP55ZR$$$EERdMyr<^^
      ''''   "}omP3oVuucVVVVyyVVwzwywIeIjjIssmKKP5GGPZdOdZbdOOdbZqIxr^
      '''  ':}wesyulVwwwkXoojyyyyywwkjUIImKK33GMMZZbdO6666OOOOO6Obqx' 
      _____=}jewuTlVyjIUeK33eXozkkkXIe3W5ZZMMMZZZZdOEERDDDEEEE0gg$EG= `);
    };
  }

  render() {
    return (
      <div className="App">
        <Router>
          <MobileBlocker>
            Mobile devices aren't supported
          </MobileBlocker>
          <OnlyDesktop>
            <DragArea active={this.state.dragCounter > 0}></DragArea>
            <AnimatedSwitch
              atEnter={{ opacity: 0 }}
              atLeave={{ opacity: 0 }}
              atActive={{ opacity: 1 }}
              className="route-wrapper"
            >
              {routes.map((route) => (
                <Route key={route.name} {...route} />
              ))}
            </AnimatedSwitch>
          </OnlyDesktop>
        </Router>
      </div>
    );
  }
}

export default App;
