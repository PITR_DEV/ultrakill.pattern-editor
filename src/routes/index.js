import * as Routes from "./routes";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Routes.Home,
    exact: true,
  },
  {
    path: "/editor/import/:pattern",
    name: "Editor",
    component: Routes.Editor,
  },
  {
    path: "/editor/:tab",
    name: "Editor",
    component: Routes.Editor,
  },
];

export default routes;
