const path = require("path");
const webpack = require("webpack");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

/**
 * @type {import('webpack').Configuration}
 */
module.exports = {
  resolve: {
    extensions: [".jsx", ".js"],
  },
  plugins: ["babel-plugin-styled-components"],
  entry: "./src/App.jsx",
  devtool: "source-map",
  output: {
    filename: "static/js/[name].[contenthash:4].js",
    chunkFilename: "static/js/[chunkhash:4][name].[contenthash:4].js",
    publicPath: "/",
    path: path.resolve(__dirname, "./dist/client"),
  },
};
