import React from "react";
import styled from "styled-components";
import { PrimaryButton, PrefabButton } from "../style/Buttons";
import { HiddenWrapper, InputField } from "../style/Forms";

import NoneIcon from "../icons/None.png";
import MeleeIcon from "../icons/Filth.png";
import ProjectileIcon from "../icons/Shotgun_Husk.png";
import JumpPadIcon from "../icons/JumpPad.png";
import StairsIcon from "../icons/Stairs.png";
import HideousIcon from "../icons/Hideous_Mass.png";

const PrefabIcon = styled.img`
  height: 32px;
  width: 32px;
`;

export default class ModifiersPanel extends React.Component {
  state = {
    mode: "",
  };

  componentDidMount() {
    this.setMode(this.props.mode);
  }

  componentDidUpdate() {
    if (this.props.mode !== this.state.mode) {
      this.setMode(this.props.mode);
    }
  }

  setMode = (mode) => {
    if (mode === "prefabs") {
      this.props.setPrefab("0");
    } else {
      this.props.setActiveModifier("plus-one");
    }
    this.setState({ mode });
  };

  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          marginLeft: "15px",
          marginRight: "15px",
          width: "200px",
        }}
      >
        {this.props.mode === "prefabs" ? (
          <>
            <PrefabButton
              active={this.props.prefab === "0"}
              onClick={() => this.props.setPrefab("0")}
            >
              <PrefabIcon alt="None Icon" src={NoneIcon}></PrefabIcon>None
            </PrefabButton>
            <PrefabButton
              active={this.props.prefab === "n"}
              onClick={() => this.props.setPrefab("n")}
            >
              <PrefabIcon alt="Melee Icon" src={MeleeIcon}></PrefabIcon>Melee
            </PrefabButton>
            <PrefabButton
              active={this.props.prefab === "p"}
              onClick={() => this.props.setPrefab("p")}
            >
              <PrefabIcon
                alt="Projectile Icon"
                src={ProjectileIcon}
              ></PrefabIcon>
              Projectile
            </PrefabButton>
            <PrefabButton
              active={this.props.prefab === "J"}
              onClick={() => this.props.setPrefab("J")}
            >
              <PrefabIcon alt="JumpPad Icon" src={JumpPadIcon}></PrefabIcon>Jump
              Pad
            </PrefabButton>
            <PrefabButton
              active={this.props.prefab === "s"}
              onClick={() => this.props.setPrefab("s")}
            >
              <PrefabIcon alt="Stairs Icon" src={StairsIcon}></PrefabIcon>Stairs
            </PrefabButton>
            <PrefabButton
              active={this.props.prefab === "H"}
              onClick={() => this.props.setPrefab("H")}
            >
              <PrefabIcon alt="Hideous Icon" src={HideousIcon}></PrefabIcon>
              Hideous
            </PrefabButton>
          </>
        ) : (
          <>
            <PrimaryButton
              bottomMargin
              active={this.props.activeModifier === "plus-one"}
              onClick={() => this.props.setActiveModifier("plus-one")}
            >
              Plus One
            </PrimaryButton>
            <PrimaryButton
              bottomMargin
              active={this.props.activeModifier === "minus-one"}
              onClick={() => this.props.setActiveModifier("minus-one")}
            >
              Minus One
            </PrimaryButton>
            <HiddenWrapper bottomMargin>
              <PrimaryButton
                docked={
                  this.props.activeModifier === "set-to" ? "bottom" : null
                }
                active={this.props.activeModifier === "set-to"}
                onClick={() => this.props.setActiveModifier("set-to")}
              >
                Set To
              </PrimaryButton>
              <InputField
                docked="top"
                type="number"
                min="-50"
                max="50"
                bottomMargin
                collapsed={this.props.activeModifier !== "set-to"}
                disabled={this.props.activeModifier !== "set-to"}
                value={this.props.setToAmount}
                onChange={(e) => this.props.onSetToChange(e)}
              ></InputField>
            </HiddenWrapper>
            <HiddenWrapper>
              <PrimaryButton
                docked={this.props.activeModifier === "plus" ? "bottom" : null}
                active={this.props.activeModifier === "plus"}
                onClick={() => this.props.setActiveModifier("plus")}
              >
                Plus
              </PrimaryButton>
              <InputField
                docked="top"
                type="number"
                min="-50"
                max="50"
                bottomMargin
                collapsed={this.props.activeModifier !== "plus"}
                disabled={this.props.activeModifier !== "plus"}
                value={this.props.plusAmount}
                onChange={(e) => this.props.onPlusChange(e)}
              ></InputField>
            </HiddenWrapper>
          </>
        )}
      </div>
    );
  }
}
