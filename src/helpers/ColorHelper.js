import { Bezier } from "bezier-js";

const b = new Bezier(0, 0, 0, 255, 0.6, 0, 1, 255);

const evaluateHeight = (height) => {
  const emulatedX = (height + 10) / 30;
  const line = { p1: { x: emulatedX, y: 0 }, p2: { x: emulatedX, y: 255 } };
  const intersect = b.intersects(line)[0];
  if (intersect) return intersect;
  else {
    return height > 0 ? 1 : 0;
  }
};

export const BlockColor = (height) => {
  const newColor = evaluateHeight(height) * 255;
  return `rgb(${newColor}, ${newColor}, ${newColor})`;
};

export const BlockOverlayColor = (height) => {
  if (height > 10) {
    height -= 3;
  }
  const newColor = evaluateHeight(height) * 255;
  return `rgb(${newColor}, ${newColor}, ${newColor})`;
};

export const BlockTextColor = (height, hidden) => {
  const evaluatedHeight = evaluateHeight(height);
  if (evaluatedHeight > 0.3 && evaluatedHeight < 0.7) {
    return hidden ? `rgba(255, 255, 255, 0.1)` : `rgb(255, 255, 255)`;
  } else {
    const color = (1 - evaluatedHeight) * 255;
    return hidden
      ? `rgba(${color}, ${color}, ${color}, 0.1)`
      : `rgb(${color}, ${color}, ${color})`;
  }
};
