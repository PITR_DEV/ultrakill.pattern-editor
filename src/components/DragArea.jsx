import React, { useCallback } from "react";
import styled from "styled-components";
import { useDropzone } from "react-dropzone";
import { withRouter } from "react-router-dom";

const DragArea = (props) => {
  const onDrop = useCallback((files) => {
    // This is a crazy solution
    // We're the pattern text file into base64 and then we redirect
    // to a page with it
    // The max URL size is 2000 and all patterns turned into base64 should be around or below 800 chars
    files[0]
      .text()
      .then((text) => props.history.push(`/editor/import/${btoa(text)}`));
  }, [props.history]);
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      <DragAreaStyle active={props.active}>
        Drop your pattern here
      </DragAreaStyle>
    </div>
  );
};

export default withRouter(DragArea);

const DragAreaStyle = styled.div`
  position: fixed;
  z-index: 10;
  top: 10px;
  bottom: 10px;
  left: 10px;
  right: 10px;
  border-radius: 10px;
  border: dashed red 10px;
  background-color: black;
  color: red;
  font-size: 8vw;
  text-align: center;
  font-weight: 600;
  padding: 50px;

  ${(props) => !props.active && `display: none;`}
`;
