import React from "react";
import { PrimaryButtonLink } from "../style/Buttons";
import { CenterLayout } from "../style/Layout";
import LoadButton from "../components/LoadButton";

class Home extends React.Component {
  render() {
    return (
      <>
        <CenterLayout>
          <img alt="logo" src="logo.webp" style={{ maxHeight: "80px" }}></img>
          <h2
            style={{
              color: "white",
              marginTop: "0px",
              marginBottom: "25px",
              fontWeight: 300,
            }}
          >
            Pattern Editor
          </h2>

          <PrimaryButtonLink to={"/editor/heights"} wide>
            NEW
          </PrimaryButtonLink>
          <br />
          <div style={{ width: "200px" }}>
            <LoadButton></LoadButton>
          </div>
        </CenterLayout>
      </>
    );
  }
}

export default Home;
