# ULTRAKILL Pattern Editor
The latest version is always automatically built and hosted at [cyber.pitr.dev](https://cyber.pitr.dev/)

# Building
Make sure that you have an up to date version of nodejs installed.
- run `npm install` or `yarn` to install the dependencies
- `npm run start` or `yarn start` to start the development server and
- `npm run build` or `yarn build` to make a static build
