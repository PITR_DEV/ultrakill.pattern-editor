import React from "react";
import loadable from "loadable-components";

import Loading from "../components/Loading";
const LoadingComponent = () => <Loading />;

export const Home = loadable(() => import("../pages/Home"), {
  LoadingComponent,
});

export const Editor = loadable(() => import("../pages/Editor"), {
  LoadingComponent,
});
