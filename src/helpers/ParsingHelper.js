// I hate how weakly typed everything here is.
// TODO I should probably switch this whole project over to TypeScript

const arenaSize = 16;

export const Import = async (text, callback) => {
  // await to resolve the promise

  console.log(text);
  var lineSplit = text.split("\n");
  if (lineSplit.length !== arenaSize * 2 + 1) {
    return console.error(
      `Invalid size. ${lineSplit.length} instead of ${arenaSize * 2 + 1}`
    );
  }

  let hgrid = [];
  for (let y = 0; y < arenaSize; y++) {
    hgrid[y] = [];
    for (let x = 0; x < arenaSize; x++) {
      hgrid[y][x] = { x: x, y: y, height: undefined };
    }
  }

  // Actually parse shit
  for (let y = 0; y < arenaSize; y++) {
    let containerOpened = false;
    let containerBuilder = "";
    let adjustedX = 0;
    for (let x = 0; x < lineSplit[y].length; x++) {
      if (lineSplit[y][x] === "(") {
        containerOpened = true;
        continue;
      }

      if (lineSplit[y][x] === ")") {
        if (containerOpened) {
          console.log(`Parsing ${containerBuilder}`);
          hgrid[y][adjustedX].height = parseInt(containerBuilder);
          containerBuilder = "";
          containerOpened = false;
          adjustedX++;
          continue;
        } else {
          return console.error(`Invalid container at ${y} ${x}`);
        }
      }

      if (containerOpened) {
        containerBuilder += lineSplit[y][x];
      } else {
        if (!isNaN(parseInt(lineSplit[y][x]))) {
          console.log(adjustedX);
          hgrid[y][adjustedX].height = parseInt(lineSplit[y][x]);
          adjustedX++;
        }
      }
    }
  }

  // height parsing done

  let pgrid = [];
  for (let y = 0; y < arenaSize; y++) {
    pgrid[y] = [];
    for (let x = 0; x < arenaSize; x++) {
      pgrid[y][x] = { x: x, y: y, prefab: undefined };
    }
  }

  // Parse the prefabs
  for (let y = 0; y < arenaSize; y++) {
    for (let x = 0; x < arenaSize; x++) {
      pgrid[y][x].prefab = lineSplit[y + arenaSize + 1][x];
    }
  }

  callback({ heights: hgrid, prefabs: pgrid });
};

export const Export = (data) => {
  //data.heights
  //data.prefabs

  let stringBuilder = "";

  for (let y = 0; y < arenaSize; y++) {
    for (let x = 0; x < arenaSize; x++) {
      if (data.heights[y][x].height < 0 || data.heights[y][x].height > 9) {
        stringBuilder += `(${data.heights[y][x].height})`;
      } else {
        stringBuilder += data.heights[y][x].height;
      }
    }
    stringBuilder += "\n";
  }
  stringBuilder += "\n";

  for (let y = 0; y < arenaSize; y++) {
    for (let x = 0; x < arenaSize; x++) {
      stringBuilder += data.prefabs[y][x].prefab;
    }
    if (y !== arenaSize - 1) stringBuilder += "\n";
  }

  console.log(stringBuilder);


  // I hate this solution, but it works
  // Feel free to open a pull request if you know a better one :^)
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(stringBuilder)
  );

  const date = new Date();

  element.setAttribute(
    "download",
    `${date.getHours()}:${date.getMinutes()} - ${date.getDay()}/${date.getMonth()}/${date.getFullYear()}.cgp`
  );

  element.style.display = "none";
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
};
