import styled, { css } from "styled-components";
import { Dockable, QuickMargins } from "./Layout";

export const WithCollapsedState = css`
  ${(props) =>
    props.collapsed &&
    `height: 0;
    display: none;`}
`;

export const HiddenWrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${QuickMargins}
`;

export const InputField = styled.input`
  font-size: 15px;
  border: 2px solid white;
  border-radius: 6px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 20px;
  padding-right: 20px;
  background-color: unset;
  color: white;
  outline: none;
  // cursor: text;

  ${(props) => !props.small && `min-width: 100px;`}
  ${Dockable}
  ${WithCollapsedState}

  transition: border 150ms ease-in-out, background-color 100ms ease-in-out;

  &:focus {
    border: 2px solid red;
  }
`;
