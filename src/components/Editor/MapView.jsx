import React, { Component } from "react";
import styled from "styled-components";
import { Export } from "../../helpers/ParsingHelper";
import { GridButton } from "../../style/Buttons";
import StairsIcon from "../../icons/Stairs_Map_Preview.svg";
import StairsIconBlack from "../../icons/Stairs_Map_Preview_Black.svg";

const StairsIconStyle = styled.img`
  position: absolute;
  left: 2px;
  bottom: 2px;
  height: 9px;
  width: 9px;
  opacity: 0.2;
`;

let prefabGrid = [];
let heightGrid = [];

export default class MapView extends Component {
  state = {
    mode: "heights",
    grid: [],
  };

  loadPattern = (pattern) => {
    heightGrid = pattern.heights;
    prefabGrid = pattern.prefabs;
    this.setState({ grid: heightGrid, mode: "heights" });
  };

  exportPattern = () => {
    Export({ heights: heightGrid, prefabs: prefabGrid });
  };

  componentDidMount() {
    let hgrid = [];
    for (let y = 0; y < 16; y++) {
      hgrid[y] = [];
      for (let x = 0; x < 16; x++) {
        hgrid[y][x] = { x: x, y: y, height: 0 };
      }
    }
    heightGrid = hgrid;
    let pgrid = [];
    for (let y = 0; y < 16; y++) {
      pgrid[y] = [];
      for (let x = 0; x < 16; x++) {
        pgrid[y][x] = { x: x, y: y, prefab: "0" };
      }
    }
    prefabGrid = pgrid;
    if (this.props.mode === "heights") {
      this.setState({ grid: heightGrid, mode: "heights" });
    } else {
      this.setState({ grid: prefabGrid, mode: "prefabs" });
    }
  }

  componentDidUpdate() {
    if (this.props.mode !== this.state.mode) {
      let newGrid = [];
      if (this.props.mode === "heights") {
        prefabGrid = this.state.grid;
        newGrid = heightGrid;
      } else {
        heightGrid = this.state.grid;
        newGrid = prefabGrid;
      }

      this.setState({ mode: this.props.mode, grid: newGrid });
    }
  }

  onBlockClicked = (x, y) => {
    switch (this.props.activeTool) {
      default:
        break;
      case "point":
        this.affectBlock(x, y);
        break;
      case "rect":
        if (this.props.rectPointA) {
          const lowestY =
            this.props.rectPointA.y > y ? y : this.props.rectPointA.y;
          const highestY =
            this.props.rectPointA.y > y ? this.props.rectPointA.y : y;
          const lowestX =
            this.props.rectPointA.x > x ? x : this.props.rectPointA.x;
          const highestX =
            this.props.rectPointA.x > x ? this.props.rectPointA.x : x;
          for (let loopY = lowestY; loopY <= highestY; loopY++) {
            for (let loopX = lowestX; loopX <= highestX; loopX++) {
              this.affectBlock(loopX, loopY);
            }
          }
          this.props.resetPoint();
        } else {
          this.props.selectPoint(x, y);
        }
        break;
    }
  };

  affectBlock = (x, y) => {
    var temp = this.state.grid;

    switch (this.props.activeModifier) {
      default:
        break;
      case "plus-one":
        if (temp[y][x].height < 50) temp[y][x].height++;
        break;
      case "minus-one":
        if (temp[y][x].height > -50) temp[y][x].height--;
        break;
      case "set-to":
        if (!isNaN(parseInt(this.props.setToAmount)))
          temp[y][x].height = parseInt(this.props.setToAmount);
        break;
      case "plus":
        var plusInt = parseInt(this.props.plusAmount);
        if (
          !isNaN(plusInt) &&
          temp[y][x].height + plusInt <= 50 &&
          temp[y][x].height + plusInt >= -50
        )
          temp[y][x].height += plusInt;
        break;
      case "prefab":
        temp[y][x].prefab = this.props.activePrefab;
        break;
    }
    this.setState({ grid: temp });
  };

  render() {
    return (
      <div
        style={{
          userSelect: "none",
          flexGrow: 1,
          maxWidth: "700px",
        }}
      >
        <div
          style={{
            position: "relative",
            paddingTop: "100%",
          }}
        >
          <div
            style={{
              position: "absolute",
              top: "0px",
              bottom: "0px",
              left: "0px",
              right: "0px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            {this.state.grid.map((row, index) => (
              <div
                key={`row ${index}`}
                style={{ display: "flex", flexDirection: "row", flex: 1 }}
              >
                {row.map((block) => (
                  <GridButton
                    key={`block ${block.x}-${block.y}`}
                    selected={
                      this.props.rectPointA &&
                      this.props.rectPointA.x === block.x &&
                      this.props.rectPointA.y === block.y
                    }
                    height={heightGrid[block.y][block.x].height}
                    prefab={block.prefab}
                    onClick={() => {
                      this.onBlockClicked(block.x, block.y);
                    }}
                  >
                    {block.prefab ? block.prefab : block.height}
                    {prefabGrid[block.y][block.x].prefab === "s" && (
                      <StairsIconStyle
                        src={
                          heightGrid[block.y][block.x].height < 2
                            ? StairsIcon
                            : StairsIconBlack
                        }
                      ></StairsIconStyle>
                    )}
                  </GridButton>
                ))}
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
