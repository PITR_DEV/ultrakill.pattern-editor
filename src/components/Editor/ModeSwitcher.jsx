import React from "react";
import { Tab } from "../../style/Buttons";
import { MainWrapper } from "../../style/Layout";

export const ModeSwitcher = (props) => {
  return (
    <MainWrapper>
      <Tab
        onClick={() => props.history.replace("/editor/heights")}
        active={props.mode === "heights"}
        docked="top"
      >
        Heights
      </Tab>
      <Tab
        onClick={() => props.history.replace("/editor/prefabs")}
        active={props.mode === "prefabs"}
        docked="top"
      >
        Prefabs
      </Tab>
      <Tab onClick={() => props.export()} docked="top">
        Export
      </Tab>
    </MainWrapper>
  );
};
