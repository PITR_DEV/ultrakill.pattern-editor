import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { withRouter } from "react-router-dom";
import { PrimaryButton } from "../style/Buttons";

const DragArea = (props) => {
  const onDrop = useCallback(
    (files) => {
      // This is a crazy solution
      // We're the pattern text file into base64 and then we redirect
      // to a page with it
      // The max URL size is 2000 and all patterns turned into base64 should be around or below 800 chars
      files[0]
        .text()
        .then((text) => props.history.push(`/editor/import/${btoa(text)}`));
    },
    [props.history]
  );
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <div  {...getRootProps()}>
      <input  {...getInputProps()} />
      <PrimaryButton style={{width: "100%"}}>LOAD</PrimaryButton>
    </div>
  );
};

export default withRouter(DragArea);
