import styled, { css } from "styled-components";

export const QuickMargins = css`
  ${(props) => props.bottomMargin && `margin-bottom: 10px;`}
  ${(props) => props.topMargin && `margin-top: 10px;`}
`;

export const MainWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  flex-grow: 1;
`;

export const OverMainWrapper = styled.div`
  width: 100vw;
`;

export const CenterLayout = styled.div`
  display: flex;
  position: fixed;
  bottom: 0px;
  top: 0px;
  right: 0px;
  left: 0px;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Panel = styled.div`
  border: 2px solid rgba(255, 255, 255, 0.6);
  border-radius: 5px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 10px;
  padding-right: 10px;
  background-color: unset;
  ${QuickMargins}

  ${(props) =>
    props.column &&
    `
      display: flex;
      flex-direction: column;
  `}
`;

export const MobileBlocker = styled.div`
  display: none;
  @media only screen and (max-width: 630px) {
    display: flex;
    position: absolute;
    z-index: 1000000;
    top: 0px;
    bottom: 0px;
    left: 0px;
    right: 0px;
    background-color: black;
    justify-content: center;
    align-items: center;
    font-size: 16px;
  }
`;

export const OnlyDesktop = styled.div`
  @media only screen and (max-width: 630px) {
    display: none;
  }
`;

export const Dockable = css`
  ${(props) =>
    props.docked === "bottom"
      ? `border-radius: 6px 6px 0px 0px;`
      : props.docked === "top" && `border-radius: 0px 0px 6px 6px;`}
`;
