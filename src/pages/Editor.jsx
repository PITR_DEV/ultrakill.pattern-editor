import React, { Component } from "react";
import { MainWrapper, OverMainWrapper } from "../style/Layout";
import MapView from "../components/Editor/MapView";
import { ModeSwitcher } from "../components/Editor/ModeSwitcher";
import { PrimaryButton } from "../style/Buttons";
import { CreateOutlined, AspectRatioOutlined } from "@material-ui/icons";
import { withRouter } from "react-router-dom";
import ModifiersPanel from "../components/ModifiersPanel";
import { Import } from "../helpers/ParsingHelper";

class Editor extends Component {
  constructor(props) {
    super(props);
    this.mapRef = React.createRef();
  }

  state = {
    activePrefab: "0",
    activeModifier: "plus-one",
    activeTool: "point",
    rectPointA: null,
    setToAmount: 0,
    plusAmount: 2,
  };

  prettyPleaseExportThePattern = () => {
    this.mapRef.current.exportPattern();
  };

  // TODO Switch to a more modern solution
  // I'm still used to older react, it changes so darn quickly
  componentDidUpdate() {
    if (this.props.match.params.pattern) {
      Import(atob(this.props.match.params.pattern), (pattern) => {
        this.mapRef.current.loadPattern(pattern);
        this.props.history.replace("/editor/heights");
      });
    }
  }

  render() {
    return (
      <div>
        <ModeSwitcher
          export={this.prettyPleaseExportThePattern}
          history={this.props.history}
          mode={this.props.match.params.tab}
        ></ModeSwitcher>
        <OverMainWrapper>
          <MainWrapper>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginRight: "15px",
                marginLeft: "15px",
              }}
            >
              <PrimaryButton
                active={this.state.activeTool === "point"}
                onClick={() =>
                  this.setState({ activeTool: "point", rectPointA: null })
                }
                small
                bottomMargin
              >
                <CreateOutlined></CreateOutlined>
              </PrimaryButton>
              <PrimaryButton
                active={this.state.activeTool === "rect"}
                onClick={() =>
                  this.setState({ activeTool: "rect", rectPointA: null })
                }
                small
                bottomMargin
              >
                <AspectRatioOutlined></AspectRatioOutlined>
              </PrimaryButton>
            </div>
            <MapView
              ref={this.mapRef}
              mode={this.props.match.params.tab}
              activePrefab={this.state.activePrefab}
              activeTool={this.state.activeTool}
              activeModifier={this.state.activeModifier}
              selectPoint={(x, y) => {
                this.setState({ rectPointA: { x, y } });
              }}
              resetPoint={() => this.setState({ rectPointA: null })}
              rectPointA={this.state.rectPointA}
              setToAmount={this.state.setToAmount}
              plusAmount={this.state.plusAmount}
            ></MapView>
            <ModifiersPanel
              setPrefab={(p) =>
                this.setState({ activePrefab: p, activeModifier: "prefab" })
              }
              prefab={
                this.state.activeModifier === "prefab"
                  ? this.state.activePrefab
                  : ""
              }
              mode={this.props.match.params.tab}
              activeModifier={this.state.activeModifier}
              setActiveModifier={(modifier) =>
                this.setState({ activeModifier: modifier })
              }
              setToAmount={this.state.setToAmount}
              onSetToChange={(e) => {
                var setToAmount = e.target.value;
                var parsed = parseInt(setToAmount);
                if (!isNaN(parsed)) {
                  if (parsed > 50) setToAmount = "50";
                  else if (parsed < -50) setToAmount = "-50";
                }
                this.setState({ setToAmount });
              }
              }
              plusAmount={this.state.plusAmount}
              onPlusChange={(e) => {
                var plusAmount = e.target.value;
                var parsed = parseInt(plusAmount);
                if (!isNaN(parsed)) {
                  if (parsed > 50) plusAmount = "50";
                  else if (parsed < -50) plusAmount = "-50";
                }
                this.setState({ plusAmount });
              }
              }
            ></ModifiersPanel>
          </MainWrapper>
        </OverMainWrapper>
      </div>
    );
  }
}

export default withRouter(Editor);
